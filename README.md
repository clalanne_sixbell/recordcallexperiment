
- [Overview](#overview)
- [Compilation](#compilation)
  - [OSX](#osx)
  - [Linux](#linux)
- [Execution](#execution)


# Overview
# Compilation
## OSX
```bash
g++ -std=c++17 main.cpp -I /Library/Frameworks/GStreamer.framework/Versions/1.0/Headers -F /Library/Frameworks/ -framework GStreamer -o recordcall
```

## Linux
If centos7 do the following first
```bash
scl enable devtoolset-8 zsh
```

### Ubuntu
```bash
g++ -std=c++17 main.cpp -o recordcall -I /usr/include/gstreamer-1.0 -I /usr/include/glib-2.0 -I /usr/lib/x86_64-linux-gnu/glib-2.0/include -lgstreamer-1.0 -lgobject-2.0 -lglib-2.0
```

### Centos7
```bash
g++ -std=c++17 main.cpp -o recordcall -I /usr/include/gstreamer-1.0 -I /usr/include/glib-2.0 -I /usr/lib/x86_64-linux-gnu/glib-2.0/include -I /usr/lib64/glib-2.0/include  -lgstreamer-1.0 -lgobject-2.0 -lglib-2.0
```

# Execution
```bash
./recordcall
```
