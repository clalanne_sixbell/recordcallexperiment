
#include <gst/gst.h>

#include <iostream>

const int SUCCESS = 0;
const int FAIL = -1;

gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data) {
  GMainLoop *loop = (GMainLoop *)data;

  switch (GST_MESSAGE_TYPE(msg)) {
    case GST_MESSAGE_EOS:
      g_print("End of stream\n");
      g_main_loop_quit(loop);
      break;

    case GST_MESSAGE_ERROR: {
      gchar *debug;
      GError *error;

      gst_message_parse_error(msg, &error, &debug);
      g_free(debug);

      g_printerr("Error: %s\n", error->message);
      g_error_free(error);

      g_main_loop_quit(loop);
      break;
    }
    default: {
      // std::cerr << "[bus_call] type of message not known\n";
      break;
    }
  }

  return TRUE;
}

int main(int argc, char *argv[]) {
  GstMessage *msg;

  gst_init(&argc, &argv);

  auto pipeline = gst_pipeline_new("record_call");
  if (pipeline == nullptr) {
    std::cerr << "not able to build a pipeline\n";
    return FAIL;
  }

  auto loop = g_main_loop_new(NULL, FALSE);
  if (loop == nullptr) {
    std::cerr << "not able to build a pipeline\n";
    return FAIL;
  }

  auto source1 = gst_element_factory_make("udpsrc", "udpsrc1");
  auto source2 = gst_element_factory_make("udpsrc", "udpsrc2");
  auto resample1 = gst_element_factory_make("audioresample", "resample1");
  auto resample2 = gst_element_factory_make("audioresample", "resample2");
  // auto resample = gst_element_factory_make("audioresample", "resample");
  auto conv1 = gst_element_factory_make("audioconvert", "conv1");
  auto conv2 = gst_element_factory_make("audioconvert", "conv2");
  auto mixer = gst_element_factory_make("audiomixer", "mixer");
  auto wavencoder = gst_element_factory_make("wavenc", "encoder");
  auto filesink = gst_element_factory_make("filesink", "filesink");
  auto depayloader1 = gst_element_factory_make("rtppcmudepay", "rtpdepay1");
  auto depayloader2 = gst_element_factory_make("rtppcmudepay", "rtpdepay2");
  auto rtpdecoder1 = gst_element_factory_make("mulawdec", "decoder1");
  auto rtpdecoder2 = gst_element_factory_make("mulawdec", "decoder2");
  auto queue1 = gst_element_factory_make("queue", "queue1");
  auto queue2 = gst_element_factory_make("queue", "queue2");

  if (source1 == nullptr && source2 == nullptr && /*resample == nullptr &&*/
      /*resample1 == nullptr && resample2 == nullptr && conv1 == nullptr &&
      conv2 == nullptr &&*/
      mixer == nullptr && wavencoder == nullptr && filesink == nullptr &&
      depayloader1 == nullptr && depayloader2 == nullptr &&
      rtpdecoder1 == nullptr && queue1 == nullptr && queue2 == nullptr &&
      rtpdecoder2 == nullptr) {
    std::cerr << "not able to build an element\n";
    return FAIL;
  }

  g_object_set(G_OBJECT(filesink), "location", "record_call.wav", NULL);
  g_object_set(G_OBJECT(mixer), "name", "m_adder", NULL);
  g_object_set(G_OBJECT(source1), "address", "127.0.0.1", NULL);
  g_object_set(G_OBJECT(source2), "address", "127.0.0.1", NULL);
  g_object_set(G_OBJECT(source1), "port", 9004, NULL);
  g_object_set(G_OBJECT(source2), "port", 9008, NULL);
  GstCaps *caps = gst_caps_from_string(
      "application/x-rtp, media=(string)audio, clock-rate=(int)8000, "
      "encoding-name=(string)PCMU, payload=(int)0");
  if (caps == nullptr) {
    std::cerr << "not able to create caps for source\n";
    return FAIL;
  }
  g_object_set(G_OBJECT(source1), "name", "source A", NULL);
  g_object_set(G_OBJECT(source1), "caps", caps, NULL);

  GstCaps *caps1 = gst_caps_from_string(
      "application/x-rtp, media=(string)audio, clock-rate=(int)8000, "
      "encoding-name=(string)PCMU, payload=(int)0");
  if (caps1 == nullptr) {
    std::cerr << "not able to create caps for source\n";
    return FAIL;
  }

  g_object_set(G_OBJECT(source2), "caps", caps1, NULL);
  g_object_set(G_OBJECT(source2), "name", "source B", NULL);

  auto bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
  if (bus == nullptr) {
    std::cerr << "not able to build a bus\n";
    return FAIL;
  }

  gst_bin_add_many(
      GST_BIN(pipeline), source1, source2, /*resample1, resample2,*/
      /*resample,*/ /*conv1, conv2,*/ mixer, wavencoder, filesink, queue1,
      queue2, depayloader1, depayloader2, rtpdecoder1, rtpdecoder2, NULL);

  /**********************/
  if (!gst_element_link(source1, queue1)) {
    std::cerr << "not able to link source1 with queue1\n";
    return FAIL;
  }

  if (!gst_element_link(queue1, depayloader1)) {
    std::cerr << "not able to link queue1 with depayloader1\n";
    return FAIL;
  }

  if (!gst_element_link(depayloader1, rtpdecoder1)) {
    std::cerr << "not able to link depayloader1 with rtpdecoder1\n";
    return FAIL;
  }

  /*if (!gst_element_link(rtpdecoder1, conv1)) {
    std::cerr << "not able to link rtpdecoder1 with conv1\n";
    return FAIL;
  }
  if (!gst_element_link(conv1, resample1)) {
    std::cerr << "not able to link conv1 with resample1\n";
    return FAIL;
  }*/

  /*****************/

  if (!gst_element_link(source2, queue2)) {
    std::cerr << "not able to link source2 with queue2\n";
    return FAIL;
  }

  if (!gst_element_link(queue2, depayloader2)) {
    std::cerr << "not able to link queue2 with depayloader2\n";
    return FAIL;
  }

  if (!gst_element_link(depayloader2, rtpdecoder2)) {
    std::cerr << "not able to link depayloader2 with rtpdecoder2\n";
    return FAIL;
  }

  /*if (!gst_element_link(rtpdecoder2, conv2)) {
    std::cerr << "not able to link rtpdecoder2 with conv2\n";
    return FAIL;
  }
  if (!gst_element_link(conv2, resample2)) {
    std::cerr << "not able to link conv2 with resample2\n";
    return FAIL;
  }*/

  /*****************/
  if (!gst_element_link(mixer, wavencoder)) {
    std::cerr << "not able to link mixer with resample\n";
    return FAIL;
  }

  /*if (!gst_element_link(resample, wavencoder)) {
    std::cerr << "not able to link resample with wavencoder\n";
    return FAIL;
  }*/

  if (!gst_element_link(wavencoder, filesink)) {
    std::cerr << "not able to link wavencoder with filesink\n";
    return FAIL;
  }
  /*****************/

  auto resample1_pad = gst_element_get_static_pad(rtpdecoder1, "src");
  auto resample2_pad = gst_element_get_static_pad(rtpdecoder2, "src");

  auto adder_sinkpad1 = gst_element_get_request_pad(mixer, "sink_%u");
  auto pad1name = gst_pad_get_name(adder_sinkpad1);
  g_print("pad1name: %s\n", pad1name);
  auto adder_sinkpad2 = gst_element_get_request_pad(mixer, "sink_%u");
  auto pad2name = gst_pad_get_name(adder_sinkpad2);
  g_print("pad2name: %s\n", pad2name);

  int i, j;
  if ((i = gst_pad_link(resample1_pad, adder_sinkpad1)) != 0) {
    g_print("pad error: %d\n", i);
    g_print("cannot link conv1 with adder1\n");
  }

  if ((j = gst_pad_link(resample2_pad, adder_sinkpad2)) != 0) {
    g_print("pad2 error: %d\n", j);
    g_print("cannot link conv2 with adder2\n");
  }

  std::cout << "Now playing the pipeline!!!!\n";
  auto ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);
  if (ret != GST_STATE_CHANGE_SUCCESS && ret != GST_STATE_CHANGE_ASYNC) {
    std::cerr << "not able to start pipeline [" << ret << "]\n";
    return FAIL;
  }

  auto bus_watch_id = gst_bus_add_watch(bus, bus_call, loop);
  if (bus == 0) {
    std::cerr << "not able to build a watch ID\n";
    return FAIL;
  }
  gst_object_unref(bus);

  std::cout << "Running loop!!!!\n";
  g_main_loop_run(loop);
  std::cout << "closing everything!!!!\n";

  return SUCCESS;
}